# Pairing exercise

## Instructions for the interview candidate

You'll be pair-programming with one or two Sedex engineers on a refactoring task. 

**What we're looking for ("must-have" criteria)**:

* Being able to communicate effectively in a pairing session
* Being able to read existing code effectively
* Being able to make small changes in a safe manner
* Being able to write correct, simple, expressive code

Please treat this task as you would in your day job.

Feel free to look up docs, APIs, or search on Google/StackOverflow.

Completion of the whole task is NOT a must-have. We're looking for the approach you take, not how fast you can do it.

Feel free to ask any questions at any point.

### The task

We have some code and tests that are part of a larger codebase. The code works, but it's been written in a hurry and needs some cleaning up. The developer who wrote it no longer works here, so we have to interpret the code ourselves.

**Your task is to refactor the existing code to make it simpler and more readable**, in preparation for adding some features later on. 

You can change the types, signatures, interfaces etc. to make the code better. You can even update the tests or write new ones if you want to. 

### Usage

You need IntelliJ IDEA to run this project (and a JDK installed).